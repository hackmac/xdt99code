XDTCode: An IDE for developing software for TI-99/4A
========================================

**[XDT99Code][13]** is a split of the example application SimpleXDT99IDE from the project [XDT99-Wrapper][2].
This project implements an IDE for the console based tools for cross platform development of (GPL-)Assembler and Basic programs for the TI-99/4A under macOS.   
Compared to the sample IDE, this IDE has a text editor that behaves similar to the Editor/Assembler of the TI-99/4a, and better error handling, coloured syntax highlighting, improved navigation within the source code, improved handling of projects with multiple files and a feature for executing XDT99 commands in the terminal and much more... (XDT99Code needs macOS 10.10 and later to run.)   

The complete source of the XDT99Code and its sample App is available on [Bitbucket][13] and is released under the [GNU General Public License, Version 2.0][1] (GNU GPLv2.0).


Download and Installation
-------------------------

Clone the entire XDTCode bitbucket [repository][13] and checkout the [master][4] on the master branch. To open the project files, you will need to install the actual version of XCode from Apples AppStore before.  
If you want to use the HEAD of the branch (instead any of the tags), a submodule is also necessary for a successful build: My custom branch of [NoodleKit][9] (which is forked from [MrNoodle][10]) provides a NoodleLineNumberView class for using line numbering in the source code editor.

The XDT99-Wrapper classes are already included with the project, by the [release (candidate)][3] 0.5 of the XDTools99.framework. That release also contains the python based tools from the [xdt99 project][6] (release [2.0.1][7] including [refactoring patches](https://github.com/endlos99/xdt99/commit/9ca75317e872800b62d732e712fcfe2441195965).


Contact Information
-------------------

The XDT99Code are released under the GNU GPLv2.0, in the hope that Mac and TI-99 enthusiasts may find them useful.

Please report feedback and all bugs to the [developer][11] by [creating][5] an issue at bitbucket.

[1]: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
[2]: https://github.com/endlos99/xdt99-wrapper
[3]: https://github.com/endlos99/xdt99-wrapper
[4]: https://bitbucket.org/hackmac/xdt99code/branch/master
[5]: https://bitbucket.org/hackmac/xdt99code/issues?status=new&status=open
[6]: https://github.com/endlos99/xdt99
[7]: https://github.com/endlos99/xdt99/releases/tag/2.0.1
[8]: https://github.com/henrik-w
[9]: https://github.com/henrik-w/NoodleKit
[10]: https://github.com/MrNoodle/NoodleKit
[11]:https://bitbucket.org/hackmac
[12]:https://bitbucket.org/hackmac/tidisk-manager
[13]:https://bitbucket.org/hackmac/xdt99code
